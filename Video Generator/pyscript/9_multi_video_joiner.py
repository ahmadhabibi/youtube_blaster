import os
import random
import json
import sys
from datetime import datetime
import subprocess
import time

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])

CSV_MULTI = os.path.join(cwd,config["folder"][0]['csvmulti'])
FOLDER_VIDEO_MULTI = os.path.join(cwd,config["folder"][0]['foldermulti'])

FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def runBash(command):
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)
    
"""MAIN SCRIPT"""
f = open(CSV_MULTI, "r")
a = f.readlines()[1:]
f.close()

if not os.path.exists(FOLDER_VIDEO_MULTI):
    os.makedirs(FOLDER_VIDEO_MULTI)

def proses(nm,video):
    path_file_txt = os.path.join(FOLDER_VIDEO_MULTI,"{}.txt".format(nm))
    path_file_video = os.path.join(FOLDER_VIDEO_MULTI,"{}.mp4".format(nm))

    f = open(path_file_txt, "w")
    v = video.split("|")
    for vid in v:
        strteks = f"file '{vid}'\n"
        f.write(strteks)
    f.close()
    
    tulis_log("Multi_video_joiner.exe | Berhasil membuat file {}".format(path_file_txt))
    strbash = FFMPEG+' -y -f concat -safe 0 -i "{}" -c copy "{}"'.format(path_file_txt,path_file_video)
    runBash(strbash)
    tulis_log("Multi_video_joiner.exe | Render video : {}".format(path_file_video))
    time.sleep(3)
    os.remove(path_file_txt)
    tulis_log(f"Multi_video_joiner.exe | Berhasil membuat video {path_file_video}")

for line in a:
    kolom = line.split(";")
    video = kolom[0].replace("\n","")
    nama_file = kolom[1].replace("\n","")
    proses(nama_file,video)

tulis_log("Done !!!")