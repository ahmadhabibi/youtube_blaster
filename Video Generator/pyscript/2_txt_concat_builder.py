import os
import random
import json
import sys
from datetime import datetime

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])
FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
list_video = []
if os.path.exists(FOLDER_VIDEO_PECAH):
    baca_folder = os.listdir(FOLDER_VIDEO_PECAH)
    for v in baca_folder:
        if ".mp4" in v:
            pathbaru = "{}\{}".format(FOLDER_VIDEO_PECAH,v)
            list_video.append(pathbaru)
    if not os.path.exists(FOLDER_TXT):
        os.makedirs(FOLDER_TXT)
else:
    tulis_log("Txt_concat_builder.exe | Folder bahan video tidak ada !!!")

def create_txt(namafile):
    f = open(namafile, "w")
    ulang = random.randrange(5,9)
    for a in range(1,ulang):
        str = "file '"+random.choice(list_video)+"'"
        f.write(str)
        f.write("\n")
    f.close()

def proses(nm,jml):
    for a in range(1,jml+1):
        path_file = os.path.join(FOLDER_TXT,"{}-{}.txt".format(nm,a))
        create_txt(path_file)
        tulis_log("Txt_concat_builder.exe | Berhasil membuat file {}".format(path_file))

proses(NAMA_PROJECT,JUMLAH_VIDEO)