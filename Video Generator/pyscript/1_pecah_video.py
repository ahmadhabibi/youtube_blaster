import os
import subprocess
from datetime import datetime
import time
import random
import json
import sys

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])
FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
def runBash(command):
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)
    
def detik(waktu):
    pt = datetime.strptime(waktu,'%H:%M:%S.%f')
    total_seconds = pt.second + pt.minute*60 + pt.hour*3600
    return total_seconds

def waktu(detik):
    b = time.strftime('%H:%M:%S', time.gmtime(detik))
    return b

#baca durasi video
def durasi(video):
    result = subprocess.Popen([FFPROBE, video],stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    for x in result.stdout.readlines():
          if "Duration" in x:
              panjangvideo = x[12:23]
              break
    return panjangvideo

#proses pecah video 1 per 1
def crop(input,output):
    pjg = durasi(input)
    panjang_video = detik(pjg)
    pjg_target = random.randrange(7,16)
    if panjang_video < 16:
        panjang=0
    else:
        panjang = random.randrange(0,panjang_video - pjg_target)
    start = waktu(panjang)
    end = waktu(pjg_target)
    if not os.path.exists(FOLDER_VIDEO_PECAH):
        os.makedirs(FOLDER_VIDEO_PECAH)
    path = os.path.join(FOLDER_VIDEO_PECAH,output)
    str = FFMPEG+" -y -ss " + start + " -i \"" + input + "\" -t " + end + " -c copy \""+ path+"\""
    runBash(str)
    text = "Membuat video pendek : {}".format(path)
    tulis_log("Pecah_video.exe | {}".format(text))

#Triger & loop proses pecah video
def proses(path,nama,jumlah):
    jml = jumlah+1
    for i in range(1, jml):
        crop(path,"{}-{}{}".format(nama,i,".mp4"))

#baca folder -> lalu proses pecah video
if os.path.exists(FOLDER_BAHAN_VIDEO):
    baca_folder = os.listdir(FOLDER_BAHAN_VIDEO)
    mp4 = 0
    for v in baca_folder:
        if ".mp4" in v:
            mp4 = mp4+1
            path = "{}\{}".format(FOLDER_BAHAN_VIDEO,v)
            file_name = v.replace(".mp4","")
            proses(path,file_name,POTONGAN_VIDEO)
    if mp4 == 0:
        tulis_log("Pecah_video.exe | Tidak ada file .mp4 di folder {}".format(FOLDER_BAHAN_VIDEO))
else:
    tulis_log("Pecah_video.exe | Folder {} tidak ada !!!".format(FOLDER_BAHAN_VIDEO))