import os
import random
import json
import sys
import subprocess
import time
from datetime import datetime

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
CSV_WATERMARK = config['folder'][0]['csv_watermark']
START_WATERMARK = config['folder'][0]['start_watermark']
OUTPUT_CSV_WATERMARK = config['folder'][0]['output_csv_watermark']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])
FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def runBash(command):
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)

#proses watermark
def watermark_video(video,image): 
    output = os.path.basename(video)
    output = os.path.join(OUTPUT_CSV_WATERMARK, output)
    str = FFMPEG+" -y -i \""+video+"\" -i \""+image+"\" -filter_complex \"overlay=0:0\" -c:a copy -c:v libx264 -preset veryfast \""+ output+"\""
#     print(str)
    #print("Watermark video : {}".format(output))
    tulis_log("Bulk_watermark_with_csv.exe | Watermark video : {}".format(output))
    runBash(str)

# for i in list_video:
#     img = random.choice(watermark)
#     watermark_video(i,img)

"""MAIN SCRIPT"""
f = open(CSV_WATERMARK, "r")
a = f.readlines()[START_WATERMARK:]
f.close()

if not os.path.exists(OUTPUT_CSV_WATERMARK):
    os.makedirs(OUTPUT_CSV_WATERMARK)

for line in a:
    kolom = line.split(";")
    video_file = kolom[0].replace("\n","")
    watermark_file = kolom[1].replace("\n","")
    watermark_video(video_file,watermark_file)

print("Done !!!")
