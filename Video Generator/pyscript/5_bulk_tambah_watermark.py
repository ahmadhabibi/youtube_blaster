import os
import random
import json
import sys
import subprocess
import time
from datetime import datetime

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])
FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def runBash(command):
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)
    
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
watermark = []

#Baca folder watermark
if os.path.exists(GAMBAR_WATERMARK):
    baca_folder = os.listdir(GAMBAR_WATERMARK)
    for v in baca_folder:
        if ".png" in v:
            pathbaru = "{}\{}".format(GAMBAR_WATERMARK,v)
            watermark.append(pathbaru)
else:
    tulis_log("Bulk_tambah_watermark.exe | Folder png tidak ada !!!")
    
#Baca folder video
list_video = []
if os.path.exists(FOLDER_HASIL_AUDIO):
    baca_folder = os.listdir(FOLDER_HASIL_AUDIO)
    for v in baca_folder:
        if ".mp4" in v:
            pathbaru = "{}\{}".format(FOLDER_HASIL_AUDIO,v)
            list_video.append(pathbaru)
    if not os.path.exists(FOLDER_WATERMARK):
        os.makedirs(FOLDER_WATERMARK)
else:
    tulis_log("Bulk_tambah_watermark.exe | Folder bahan video tidak ada !!!")

#proses watermark
def watermark_video(video,image): 
    output = os.path.basename(video)
    output = os.path.join(FOLDER_WATERMARK, output)
    str = FFMPEG+" -y -i \""+video+"\" -i \""+image+"\" -filter_complex \"overlay=0:0\" \""+ output+"\""
    tulis_log("Bulk_tambah_watermark.exe | Watermark video : {}".format(output))
    runBash(str)

for i in list_video:
    img = random.choice(watermark)
    watermark_video(i,img)

tulis_log("Bulk_tambah_watermark.exe | Done !!!")