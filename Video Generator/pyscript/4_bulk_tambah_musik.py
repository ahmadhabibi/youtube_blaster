import os
import subprocess
from datetime import datetime
import time
import random
import json
import glob
import sys

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

NAMA_PROJECT = config['setting'][0]["namaproject"]
JUMLAH_VIDEO = config['setting'][0]['jumlahvideotarget']
POTONGAN_VIDEO = config['setting'][0]['potonganvideo']
FOLDER_BAHAN_VIDEO = os.path.join(cwd,config["folder"][0]['foldervideobahan'])
FOLDER_VIDEO_PECAH = os.path.join(cwd,config["folder"][0]['foldervideopecah'])
FOLDER_TXT = os.path.join(cwd,config["folder"][0]['foldertxt'])
FOLDER_VIDEO_RENDER = os.path.join(cwd,config["folder"][0]['folderrender'])
FOLDER_WATERMARK = os.path.join(cwd,config["folder"][0]['folderwatermark'])
GAMBAR_WATERMARK = os.path.join(cwd,config["folder"][0]['gambarwatermark'])
FOLDER_MUSIK = os.path.join(cwd,config["folder"][0]['foldermusik'])
FOLDER_HASIL_AUDIO = os.path.join(cwd,config["folder"][0]['folderhasilaudio'])
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

def runBash(command):
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

list_video = []
if os.path.exists(FOLDER_VIDEO_RENDER):
    baca_folder = os.listdir(FOLDER_VIDEO_RENDER)
    for v in baca_folder:
        if ".mp4" in v:
            pathbaru = "{}\{}".format(FOLDER_VIDEO_RENDER,v)
            list_video.append(pathbaru)
    if not os.path.exists(FOLDER_HASIL_AUDIO):
        os.makedirs(FOLDER_HASIL_AUDIO)
else:
    tulis_log("Bulk_tambah_musik.exe | Folder bahan video tidak ada !!!")

list_musik = []
if os.path.exists(FOLDER_MUSIK):
    baca_folder = os.listdir(FOLDER_MUSIK)
    for v in baca_folder:
        if ".mp3" in v:
            pathbaru = "{}\{}".format(FOLDER_MUSIK,v)
            list_musik.append(pathbaru)
else:
    tulis_log("Bulk_tambah_musik.exe | Folder bahan musik tidak ada !!!")
    
def runBash(command):
    os.system(command)

def detik(waktu):
    pt = datetime.strptime(waktu,'%H:%M:%S.%f')
    total_seconds = pt.second + pt.minute*60 + pt.hour*3600
    return total_seconds

def waktu(detik):
    b = time.strftime('%H:%M:%S', time.gmtime(detik))
    return b

#baca durasi video
def durasi(video):
    long = ""
    result = subprocess.Popen([FFPROBE, video],stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    for i in result.stdout.readlines():
        if "Duration" in i:
            long = i[12:23]
            break
    return(long)

def crop_musik(video,musikinput,musikoutput):
    pjg = durasi(video)
    panjang_video = detik(pjg)
    pjg_musik = durasi(musikinput)
    panjang_musik = detik(pjg_musik)
    start = waktu(random.randrange(0, panjang_musik - 60))
    end = waktu(panjang_video)
    if not os.path.exists(FOLDER_HASIL_AUDIO):
        os.makedirs(FOLDER_HASIL_AUDIO)
    path = os.path.join(FOLDER_HASIL_AUDIO,musikoutput)
    str = FFMPEG+" -y -ss " + start + " -i \"" + musikinput + "\" -t " + end + " -c copy \""+ path+"\""
    runBash(str)
    
def tambah_musik(video,musik):
    file_musik_baru = os.path.basename(musik).replace(".mp3","-temp.mp3")
    file_video_baru = os.path.basename(video)
    path_musik_baru = os.path.join(FOLDER_HASIL_AUDIO,file_musik_baru)
    path_video_baru = os.path.join(FOLDER_HASIL_AUDIO,file_video_baru)
    crop_musik(video,musik,file_musik_baru)
    str = FFMPEG+" -y -i \""+video+"\"  -i \""+path_musik_baru+"\" -c copy -map 0:v:0 -map 1:a:0 -shortest \""+path_video_baru+"\""
    tulis_log("Bulk_tambah_musik.exe | Menambahkan musik ke video : {}".format(path_video_baru))
    runBash(str)

for x in list_video:
    musik = random.choice(list_musik)
    tambah_musik(x,musik)
    time.sleep(1)

time.sleep(10)
tulis_log("Bulk_tambah_musik.exe | Done !!!")
if os.path.exists(FOLDER_HASIL_AUDIO):
    baca_folder = os.listdir(FOLDER_HASIL_AUDIO)
    for v in baca_folder:
        if "temp.mp3" in v:
            pathbaru = "{}\{}".format(FOLDER_HASIL_AUDIO,v)
            os.remove(pathbaru)
else:
    tulis_log("Bulk_tambah_musik.exe | Folder temp.mp3 tidak ada !!!")