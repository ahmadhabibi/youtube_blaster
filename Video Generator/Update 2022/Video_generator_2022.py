import os
import random
import json
import sys
from datetime import datetime
import subprocess
import time
import glob

cwd = os.getcwd()

#setting file config
path_config = os.path.join(cwd,"config.json")   
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

FILE_CSV = config['setting'][0]["file_csv"]
FOLDER_HASIL = config['setting'][0]["folder_hasil"]
START = config['setting'][0]["start"]
FFMPEG = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffmpeg.exe")
FFPROBE = os.path.join(config['setting'][0]['folder_ffmpeg'],"ffprobe.exe")

# ====== Function ======

def runBash(command):
    # Ini fungsinya untuk execute command ke CMD
    hasil=subprocess.Popen(command,stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    new=hasil.stdout.readlines()
    time.sleep(0.5)

def tulis_log(text):
    # Ini fungsinya untuk menulis log
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def detik(waktu):
    pt = datetime.strptime(waktu,'%H:%M:%S.%f')
    total_seconds = pt.second + pt.minute*60 + pt.hour*3600
    return total_seconds

def waktu(detik):
    b = time.strftime('%H:%M:%S', time.gmtime(detik))
    return b

#baca durasi video
def durasi(video):
    long = ""
    result = subprocess.Popen([FFPROBE, video],stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    # tulis_log(result.stdout.readlines())
    for i in result.stdout.readlines():
        x = str(i)
        if "Duration" in x:
            # tulis_log(i)
            long = x[14:25]
            break
    return(long)

# Function untuk render video dari list video
def render_join_video(nama_video,list_video):
    path_file_txt = os.path.join(FOLDER_HASIL,"{}.txt".format(nama_video))
    path_file_video = os.path.join(FOLDER_HASIL,"{}-part1.mp4".format(nama_video))

    f = open(path_file_txt, "w")
    v = list_video.split("|")
    for vid in v:
        strteks = f"file '{vid}'\n"
        f.write(strteks)
    f.close()
    
    tulis_log("Video_generator_2022.exe | Render_join_Video | Berhasil membuat file {}".format(path_file_txt))
    strbash = FFMPEG+' -y -f concat -safe 0 -i "{}" -c copy "{}"'.format(path_file_txt,path_file_video)
    runBash(strbash)
    tulis_log("Video_generator_2022.exe | Render_join_Video | Render video : {}".format(path_file_video))
    time.sleep(3)
    os.remove(path_file_txt)
    tulis_log(f"Video_generator_2022.exe | Render_join_Video | Berhasil membuat video {path_file_video}")
    time.sleep(3)


# Function untuk menambahkan audio
def crop_musik(video,musikinput,musikoutput):
    pjg = durasi(video)
    panjang_video = detik(pjg)
    pjg_musik = durasi(musikinput)
    panjang_musik = detik(pjg_musik)
    start = waktu(random.randrange(0, panjang_musik - 30))
    end = waktu(panjang_video)
    path = os.path.join(FOLDER_HASIL,musikoutput)
    str = FFMPEG+" -y -ss " + start + " -i \"" + musikinput + "\" -t " + end + " -c copy \""+ path+"\""
    runBash(str)

def tambah_musik(video,musik):
    file_musik_baru = os.path.basename(musik).replace(".mp3","-temp.mp3")
    file_video_baru = os.path.basename(video.replace("part1","part2"))
    path_musik_baru = os.path.join(FOLDER_HASIL,file_musik_baru)
    path_video_baru = os.path.join(FOLDER_HASIL,file_video_baru)
    
    crop_musik(video,musik,file_musik_baru)
    time.sleep(2)
    str = FFMPEG+" -y -i \""+video+"\"  -i \""+path_musik_baru+"\" -c copy -map 0:v:0 -map 1:a:0 -shortest \""+path_video_baru+"\""
    tulis_log("Video_generator_2022.exe | Tambah_audio | Menambahkan musik ke video : {}".format(path_video_baru))
    runBash(str)
    time.sleep(2)
    tulis_log("Video_generator_2022.exe | Tambah_audio | Berhasil menambahkan musik ke video")

#Menambahkan lowerthird video green screen #3BBD1E
def watermark_video(video,wtr,start="0"): 
    output = os.path.basename(video)
    output = os.path.join(FOLDER_HASIL, output.replace("par1","part3").replace("part2","part3"))
    str = FFMPEG+" -y -i \""+video+"\" -i \""+wtr+"\" -filter_complex \"[1:v]colorkey=0x3BBD1E:0.3:0.2[ckout];[ckout]setpts=PTS-STARTPTS+"+start+"/TB[ckout];[0:v][ckout]overlay=0:0:eof_action=pass[out]\" -map \"[out]\" -map 0:a -c:a copy \""+ output+"\""
    # print(str)
    #print("Watermark video : {}".format(output))
    tulis_log("Video_generator_2022.exe | Watermark video / lowerthird | {}".format(output))
    runBash(str)
    time.sleep(5)
    tulis_log("Video_generator_2022.exe | Watermark video / lowerthird berhasil")

#proses watermark
def watermark_gambar(video,image,start="0"): 
    output = os.path.basename(video)
    output = os.path.join(FOLDER_HASIL, output.replace("par1","part3").replace("part2","part3"))
    str = FFMPEG+" -y -i \""+video+"\" -i \""+image+"\" -filter_complex \"[1]setpts=PTS-STARTPTS+"+start+"/TB[img];[0:v][img]overlay=0:0\" -c:a copy -c:v libx264 -preset veryfast \""+ output+"\""
#     print(str)
    #print("Watermark video : {}".format(output))
    tulis_log("Video_generator_2022.exe | Watermark gambar : {}".format(output))
    runBash(str)
    time.sleep(5)
    tulis_log("Video_generator_2022.exe | Watermark gambar berhasil")

# ====== SCRIPT ======

# Membuat folder FOLDER_HASIL
if not os.path.exists(FOLDER_HASIL):
    os.makedirs(FOLDER_HASIL)

# Baca file FILE_CSV
f = open(FILE_CSV, "r")
csv = f.readlines()[START:]
f.close()

tulis_log("Video_generator_2022.exe | Start !!!")
for data in csv:
    kolom = data.split(";")
    list_video = kolom[0].replace("\n","")
    setting_audio = kolom[1].replace("\n","")
    file_audio = kolom[2].replace("\n","")
    setting_watermark = kolom[3].replace("\n","")
    file_watermark = kolom[4].replace("\n","")
    start_watermark = kolom[5].replace("\n","")
    nama_file = kolom[6].replace("\n","")
    file_hasil = os.path.join(FOLDER_HASIL,"{}.mp4".format(nama_file))
    # tulis_log(f"{list_video}\n{setting_audio}\n{file_audo}\n{setting_watermark}\n{file_watermark}\n{nama_file}")
    # tulis_log(FFMPEG)

    tulis_log(f"Video_generator_2022.exe | Memulai membuat video {file_hasil}")
    if os.path.isfile(file_hasil):
        os.remove(file_hasil)

    render_join_video(nama_file,list_video)
    video_temp = os.path.join(FOLDER_HASIL,"{}-part1.mp4".format(nama_file))
    time.sleep(5)
    if setting_audio == "1" or setting_audio == 1:
        tambah_musik(video_temp,file_audio)
        video_temp = os.path.join(FOLDER_HASIL,"{}-part2.mp4".format(nama_file))

    time.sleep(5)
    if setting_watermark == "1" or setting_watermark == 1:
        watermark_gambar(video_temp,file_watermark,start_watermark)
        video_temp = os.path.join(FOLDER_HASIL,"{}-part3.mp4".format(nama_file))
        time.sleep(5)
    elif setting_watermark == "2" or setting_watermark == 2:
        watermark_video(video_temp,file_watermark,start_watermark)
        video_temp = os.path.join(FOLDER_HASIL,"{}-part3.mp4".format(nama_file))
        time.sleep(5)
    else:
        tulis_log("Tanpa watermark")

    
    os.rename(video_temp, file_hasil)

    files_in_directory = os.listdir(FOLDER_HASIL)
    filtered_files = [file for file in files_in_directory if "part" in file or "txt" in file or "temp" in file]
    for file in filtered_files:
        path_to_file = os.path.join(FOLDER_HASIL, file)
        os.remove(path_to_file)
    tulis_log(f"Video_generator_2022.exe | Berhasil membuat video {file_hasil}")

tulis_log("Video_generator_2022.exe | Done !!!")