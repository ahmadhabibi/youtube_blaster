import json
import os
from typing import Dict, List

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.file_detector import LocalFileDetector

"""UPLOAD MODULE"""

import logging
import re
from datetime import datetime
from time import sleep
import requests

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

""" SET CONFIG FILE """
cwd = os.getcwd()

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
FILE_UPLOAD = config['setting'][0]["file_upload"]
START = config['setting'][0]["start"]
FILE_LAPORAN = config['setting'][0]["file_laporan"]
JEDA_UPLOAD = config['setting'][0]["jeda_upload"]
LIST_CHANNEL = config['setting'][0]["list_channel"]
LAPORAN_CEK_CHANNEL = config['setting'][0]["laporan_cek_channel"]
FOLDER_COOKIES = config['setting'][0]["folder_cookies"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def laporan_cookies(cookies,hasil):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{}\t{}\t{}".format(dt_string,cookies,hasil)
    f = open("laporan_cek_login.csv", "a")
    f.write(tulisan+"\n")
    f.close()
    
""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain


def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")


def confirm_logged_in(driver: WebDriver) -> bool:
    """ Confirm that the user is logged in. The browser needs to be navigated to a YouTube page. """
    try:
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.ID, "avatar-btn")))
        return True
    except TimeoutError:
        return False

def cek_login(xcookies):
    login_using_cookie_file(driver,xcookies)
    driver.get("https://www.youtube.com")
    assert "YouTube" in driver.title
    try:
        confirm_logged_in(driver)
        driver.get("https://studio.youtube.com")
        assert "Channel dashboard" in driver.title
        tulis_log("Login dengan cookies berhasil")
        laporan_cookies(xcookies,"Berhasil Login")
        driver.close()
    except:
        driver.close()
        tulis_log("Login dengan cookies gagal")
        laporan_cookies(xcookies,"Gagal Login")
    #     raise

list_cookies = []
if os.path.exists(FOLDER_COOKIES):
    baca_folder = os.listdir(FOLDER_COOKIES)
    for v in baca_folder:
        if ".json" in v:
            pathbaru = "{}\{}".format(FOLDER_COOKIES,v)
            list_cookies.append(pathbaru)
else:
    tulis_log("Bulk_render_video.exe | Folder txt tidak ada !!!")

for i in list_cookies:
    driver = webdriver.Chrome(CHROMEDRIVER)
    tulis_log("Cek cookies -> {}".format(i))
    cek_login(i)
    sleep(2)