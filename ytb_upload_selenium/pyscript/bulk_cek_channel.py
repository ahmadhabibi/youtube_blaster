import time
import csv
import os
import json
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.chrome.options import Options


""" SET CONFIG FILE """
cwd = os.getcwd()

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
FILE_UPLOAD = config['setting'][0]["file_upload"]
START = config['setting'][0]["start"]
FILE_LAPORAN = config['setting'][0]["file_laporan"]
JEDA_UPLOAD = config['setting'][0]["jeda_upload"]
LIST_CHANNEL = config['setting'][0]["list_channel"]
LAPORAN_CEK_CHANNEL = config['setting'][0]["laporan_cek_channel"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def laporan_channel(url,nama,jumlahvideo,subscribe,views,joined):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{}\t{}\t{}\t{}\t{}\t{}\t{}".format(dt_string,url,nama,jumlahvideo,subscribe,views,joined)
    f = open(LAPORAN_CEK_CHANNEL, "a")
    f.write(tulisan+"\n")
    f.close()

def jumlah_video(url):
    driver.get(url+"/videos")
    time.sleep(1)

    elem = driver.find_element_by_tag_name("body")

    lastHeight = driver.execute_script("return document.getElementById('content').scrollHeight")

    while True:
      driver.execute_script("window.scrollTo(0, document.getElementById('content').scrollHeight);")
      time.sleep(3)
      newHeight = driver.execute_script("return document.getElementById('content').scrollHeight")
      if newHeight == lastHeight:
        break
      lastHeight = newHeight
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH,"//a[@id='video-title']")))
    jumlahvideo=driver.find_elements(By.XPATH,"//a[@id='video-title']")
    return len(jumlahvideo)
    
def chanel_detail(url):
    tulis_log("Ambil data jumlah video")
    jumlahvideo = jumlah_video(url)
    time.sleep(5)
    tulis_log("Total video\t: {}".format(jumlahvideo))
    driver.get(url+"/about")
    time.sleep(1)
    
    tulis_log("Ambil data joined & total views")
    elem = driver.find_element_by_tag_name("body")
    nama = driver.find_element_by_class_name("ytd-channel-name").get_attribute('innerText')
    tulis_log("Ambil data subscriber")
    subscribe = driver.find_element_by_id("subscriber-count").get_attribute('innerText')
    tulis_log("Jumlah subscriber\t: {}".format(subscribe))
    data = driver.find_elements_by_tag_name("yt-formatted-string")
    joined = ""
    views = ""
    for i in data:
        if "views" in i.get_attribute('innerText'):
            views = i.get_attribute('innerText')
        elif "Joined" in i.get_attribute('innerText'):
            joined = i.get_attribute('innerText')
    tulis_log("Total views\t: {}".format(views))
    tulis_log("Joined\t\t: {}".format(joined))
    laporan_channel(url,nama,jumlahvideo,subscribe,views,joined)
#     driver.close()

f = open(LIST_CHANNEL, "r")
a = f.read()
f.close()

for line in a.split("\n"):
    driver = webdriver.Chrome(CHROMEDRIVER)
    tulis_log("Buka channel {}".format(line))
    chanel_detail(line)
    driver.close()
#     print(line)


tulis_log("Done !!!")
